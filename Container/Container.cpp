/*
 * Container.cpp
 *
 *  Created on: Sep 29, 2020
 *      Author: avatar
 *
 */

#include "ioi/Container/Container.hpp"


int Container::linksTwoContainers(Container *c1,Container *c2)
{
  if(c1->getFullSizeInByte() == c2->getFullSizeInByte())
    {
  c1->flush();
  c2->flush();

  c1->setg(c2->pbase(),c2->pbase(),c2->pbase());
  c2->setg(c1->pbase(),c1->pbase(),c1->pbase());
  c1->m_externGetContainer = c2;
  c2->m_externGetContainer = c1;

    }
}


Container::Container (int sizeInByte) :
    m_buffer (new char[sizeInByte]), m_sizePutSection (sizeInByte), m_sizeGetSection (
	sizeInByte)
{
  setp (m_buffer, m_buffer + m_sizePutSection);
  setg (m_buffer, m_buffer, m_buffer);
  m_elementSize = 1;
  m_numElementsInPutSection = m_sizePutSection;
  m_numElementsInGetSection = m_sizeGetSection;
}

Container::Container (int sizeElementInByte, int numElements) :
    m_buffer (new char[sizeElementInByte * numElements]), m_sizePutSection (
	sizeElementInByte * numElements), m_sizeGetSection (
	sizeElementInByte * numElements)
{

  setp (m_buffer, m_buffer + m_sizePutSection);
  setg (m_buffer, m_buffer, m_buffer);
  m_elementSize = sizeElementInByte;
  m_numElementsInPutSection = m_sizePutSection / m_elementSize;
  m_numElementsInGetSection = m_sizeGetSection / m_elementSize;
}
//Container::Container (int sizeElementInByte, int numElements,
//		      FixedBufferPointer &getFrom,IContainer *externContainer) :
//    m_buffer (new char[sizeElementInByte * numElements]), m_sizePutSection (
//	sizeElementInByte * numElements), m_sizeGetSection (
//	sizeElementInByte * numElements)
//{
//
//  m_elementSize = sizeElementInByte;
//  m_numElementsInPutSection = m_sizePutSection / m_elementSize;
//  m_numElementsInGetSection = m_sizeGetSection / m_elementSize;
//  m_externGetContainer = externContainer;
//  setp (m_buffer, m_buffer + m_sizePutSection);
//  setg (getFrom.BeginPosition, getFrom.CurrentPosition, getFrom.EndPosition);
//
//}

Container::~Container ()
{
  delete[] m_buffer;
}
int Container::getElementSize ()
{
  return m_elementSize;
}

int Container::getMaxNumElementsInBuffer ()
{
  return m_numElementsInPutSection;
}

int Container::getAvailableDataToReadInByte ()
{
  return egptr () - gptr ();
}

int Container::getAvailableMemToWriteInByte ()
{
  return epptr () - pptr ();
}

int Container::getAvailableElementToRead ()
{
  return getAvailableDataToReadInByte () / m_elementSize;
}

int Container::getAvailableElementToWrite ()
{
  return getAvailableMemToWriteInByte () / m_elementSize;
}

int Container::getFullSizeInByte ()
{
  return m_sizePutSection;
}

bool Container::isEmpty ()
{
  if (gptr () != egptr ())
    return false;
  else
    return true;

}

bool Container::isFixed ()
{
  return true;
}

bool Container::isBroken ()
{
  return false; //TODO
}

int Container::write (void *data, int lenInByte)
{
//  if(lenInByte%m_elementSize == 0)
//    {
//      if(getAvailableElementToWrite()>0)
//	{
#ifdef MULTITHREAD_CONTAINER


  if(threadID_toRead != std::this_thread::get_id())
  	  {
	std::lock_guard<std::mutex> lock(mutex);
//	std::lock_guard<std::mutex> lock(mutex_toRead);
	threadID_toWrite = std::this_thread::get_id();
	int ret = xsputn ((const char*) data, lenInByte);

	return ret;
  	  }
  else
    {
      return xsputn ((const char*) data, lenInByte);
    }
#else
  return xsputn ((const char*) data, lenInByte);
#endif
//	}
//    }
//  return EOF;

}

int Container::read (void *data, int lenInByte)
{
//  if (lenInByte % m_elementSize == 0)
//    {
//      if (getAvailableElementToRead () > 0)
//	{
#ifdef MULTITHREAD_CONTAINER
	if(threadID_toWrite != std::this_thread::get_id())
	  {
	std::lock_guard<std::mutex> lock(mutex);
//	std::lock_guard<std::mutex> lock(mutex_toRead);
	threadID_toRead = std::this_thread::get_id();
	int ret = xsgetn ((char*) data, lenInByte);
	return ret;
	  }
	else
	  {
	    return xsgetn ((char*) data, lenInByte);
	  }
#else
  return xsgetn ((char*) data, lenInByte);
#endif
//	}
//    }
//  return EOF;
}

const char* Container::dataToRead ()
{
  return gptr ();
}

char* Container::dataToWrite ()
{
  return pptr ();
}

int Container::commitWrite (int lenInByte)
{
  if (getAvailableMemToWriteInByte () >= lenInByte)
    {
      pbump (lenInByte);
      m_isPutSectionBlock = false;

      //move get section
      if (m_externGetContainer != NULL)
	m_externGetContainer->commitRead (lenInByte);
      else
	commitRead (lenInByte);

//      if (m_wasRecodedCB)
//	m_wasRecodedCB (lenInByte);
      return 0;
    }
  return EOF;
}

int Container::commitRead (int lenInByte)
{


  if (m_sizeGetSection - (gptr () - eback ()) >= lenInByte)
    {


      setg (eback (), gptr (), egptr () + lenInByte);

      if (m_wasRecodedCB)
	m_wasRecodedCB (lenInByte);
//      gbump (lenInByte);
//      m_isGetSectionBlock = false;

      return 0;
    }
  return EOF;
}

int Container::flush (eFlushBufferSections type)
{

  if (type == GET_SECTION_TO_END)
    {
      if (m_isGetSectionBlock == false)
	{
//#ifdef MULTITHREAD_CONTAINER
//	std::lock_guard<std::mutex> lock(mutex_toRead);
//#endif
	  setg (eback (), egptr (), egptr ());
	}else
	return EOF;
    }
  else if (type == GET_SECTION_TO_BEGIN)
    {
      if (m_isGetSectionBlock == false)
	{
//#ifdef MULTITHREAD_CONTAINER
//	std::lock_guard<std::mutex> lock(mutex_toRead);
//#endif
//	  printf("Flush GET_SECTION_TO_BEGIN \n");
//	  printf("%d %d %d \n",(uint64_t)eback(),(uint64_t)gptr(),(uint64_t)egptr());
	setg (eback (), eback (), eback ());
//	 printf("%d %d %d \n",(uint64_t)eback(),(uint64_t)gptr(),(uint64_t)egptr());
	}
      else
	return EOF;
    }
  else if (type == PUT_SECTION)
    {
      if (m_isPutSectionBlock == false){
//#ifdef MULTITHREAD_CONTAINER
//	std::lock_guard<std::mutex> lock(mutex_toWrite);
//#endif
//	  printf("Flush PUT_SECTION \n");
//	  printf("%d %d %d \n",(uint64_t)pbase(),(uint64_t)pptr(),(uint64_t)epptr());
	setp (m_buffer, m_buffer + m_sizePutSection);
//	printf("%d %d %d \n",(uint64_t)pbase(),(uint64_t)pptr(),(uint64_t)epptr());
      }else
	return EOF;
    }
  else
    {
      if (m_isGetSectionBlock == false && m_isPutSectionBlock == false)
	{
	  {
//#ifdef MULTITHREAD_CONTAINER
//	std::lock_guard<std::mutex> lock(mutex_toRead);
//#endif
	  setg (eback (), eback (), eback ());
	  }
	  {
//#ifdef MULTITHREAD_CONTAINER
//	std::lock_guard<std::mutex> lock(mutex_toWrite);
//#endif
	  setp (m_buffer, m_buffer + m_sizePutSection);
	  }
	}
      else
	return EOF;
    }
  return 0;
}

int Container::uflow ()
{
//  if (m_isGetSectionBlock == false)
//    {
//      if (m_elementSize == 1)
//	{
//	  if (gptr () + 1 < egptr ())
//	    {
//	      gbump (1);
//	      return *(gptr () - 1);
//	    }
//	}
//    }
  return traits_type::eof ();
}

int Container::underflow ()
{

  if (gptr () < egptr ())
    {
      return *gptr ();
    } else
    {
      //кто то вызвал проверку на переполнение а это значит использовал на пряму указатели
      //istream например
      sync();

    }
  return traits_type::eof ();
}

std::streamsize Container::showmanyc ()
{
  return epptr () - pptr ();
}

std::streamsize Container::xsgetn (char *s, std::streamsize n)
{
  if (m_isGetSectionBlock == false)
    if (n % m_elementSize == 0)
      {
	if (getAvailableElementToRead () > 0)
	  {
	    char_type *p = gptr ();
	    std::streamsize av = getAvailableDataToReadInByte ();

	    if (av == 0)
	      return traits_type::eof ();
	    if (n < av)
	      av = n;
	    for (int i = 0; i < av; i++)
	      {
		s[i] = p[i];
	      }
	    gbump (av);
	    if (m_wasReadCB)
	      m_wasReadCB (av);

	    sync();

	    return av;
	  }
      }
  return traits_type::eof ();

}

int Container::pbackfail (int c)
{
  return traits_type::eof ();
}

std::streamsize Container::xsputn (const char *s, std::streamsize n)
{


//#ifdef MULTITHREAD_CONTAINER
//  std::lock_guard<std::mutex> lock(mutex_toWrite);
//#endif

  if (m_isPutSectionBlock == false)
    if (n % m_elementSize == 0)
      {
	if (getAvailableElementToWrite () > 0)
	  {

	    char_type *p = pptr ();
	    std::streamsize av = getAvailableMemToWriteInByte ();

	    if (av == 0)
	      return traits_type::eof ();

	    if (n < av)
	      av = n;
	    for (int i = 0; i < av; i++)
	      {
		p[i] = s[i];
	      }
	    pbump (av);

	    //move get section
	    if (m_externGetContainer != NULL)
	      m_externGetContainer->commitRead (av);
	    else
	      commitRead (av);

	    return av;
	  }
      }
  return traits_type::eof ();
}

int Container::overflow (int c)
{
  return traits_type::eof ();
}

bool Container::isPutSectionBlock ()
{
  return m_isPutSectionBlock;
}

bool Container::isGetSectionBlock ()
{
  return m_isGetSectionBlock;
}

void Container::setCallback (cb_wasRead wasRead, cb_wasRecorded wasRecoded)
{
  m_wasReadCB = wasRead;
  m_wasRecodedCB = wasRecoded;
}

int Container::consumeRead (int lenInByte)
{


  if (getAvailableDataToReadInByte () >= lenInByte)
    {
//        setg (eback (), gptr (), egptr ()+lenInByte);
      gbump (lenInByte);
      m_isGetSectionBlock = false;
      sync();

      if (m_wasReadCB)
	m_wasReadCB (lenInByte);
      return 0;
    }
  return EOF;
}

std::shared_ptr<std::vector<char>> Container::readAll()
{
  std::shared_ptr<std::vector<char>> sv(new std::vector<char>((dataToRead ()),
  			dataToRead () + getAvailableDataToReadInByte ()));
  consumeRead(getAvailableDataToReadInByte());
  return sv;
}


int Container::sync ()
{
  if (m_externGetContainer)
  	{
//  	  if (m_externGetContainer->getAvailableDataToReadInByte () == 0
//  	      && !m_isPutSectionBlock
//  	      && !m_externGetContainer->isGetSectionBlock ())
  	  if (getAvailableDataToReadInByte () == 0
  	    	      && !m_externGetContainer->isPutSectionBlock()
  	    	      && !isGetSectionBlock ())
  	    {
  	      m_externGetContainer->flush (PUT_SECTION);
  	      flush (GET_SECTION_TO_BEGIN);
//  	    flush (PUT_SECTION);
//  	  m_externGetContainer->flush (GET_SECTION_TO_BEGIN);
  	    }
  	}
        else
  	{
  	  if (getAvailableDataToReadInByte () == 0 && !m_isPutSectionBlock
  	      && !m_isGetSectionBlock)
  	    {
  	      flush ();
  	    }
  	}
  return 0;


}



