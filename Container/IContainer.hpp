/*
 * IContainer.hpp
 *
 *  Created on: Sep 29, 2020
 *      Author: avatar
 */

#ifndef ICONTAINER_HPP_
#define ICONTAINER_HPP_

//#include "iostream"
#include "functional"
#include "vector"
class IContainer
{
public:

    enum eFlushBufferSections{
          ALL_SECTION,
          GET_SECTION_TO_BEGIN,
	  GET_SECTION_TO_END,
          PUT_SECTION
        };
        //call when data read
    	typedef std::function<void (int len)> cb_wasRead;
    	//call when data write
    	typedef std::function<void (int len)> cb_wasRecorded;

	virtual int getElementSize() =0;
	virtual int getMaxNumElementsInBuffer()=0;
	virtual int getAvailableDataToReadInByte()=0;
	virtual int getAvailableMemToWriteInByte()=0;
	virtual int getAvailableElementToRead()=0;
	virtual int getAvailableElementToWrite()=0;
	virtual int getFullSizeInByte()=0;
	virtual bool isEmpty()=0;
	virtual bool isFixed()=0;
	virtual bool isBroken()=0;
	virtual bool isPutSectionBlock ()=0;
	virtual bool isGetSectionBlock ()=0;

	virtual int write(void *data,int lenInByte)=0;
	virtual int read(void *data,int lenInByte)=0;
//	virtual std::vector<char>& readAll()=0;
	virtual const char* dataToRead()=0;
	virtual char* dataToWrite()=0;
	virtual int commitWrite(int lenInByte)=0;
	    /**
	    * Потверждает что появились новые данные для чтения и указатель конца get смещается на lenInByte
	    * Этот метод вызывается автоматически когда вызывается commitWrite или происходит любая запись данных
	    * @param lenInByte кол-во байт добавленных для чтения
	    * @return 0 если все хорошо -1 если нет свободного места
	    */
	virtual int commitRead(int lenInByte)=0;
	/**
	 * Потверждает что данные в get были считаны и текущий указатель gptr смещается на  lenInByte
	 * @param lenInByte кол-во байт считанных
	 * @return
	 */
	virtual int consumeRead(int lenInByte)=0;
	virtual int flush(eFlushBufferSections type=ALL_SECTION)=0;

	virtual void setCallback(cb_wasRead wasRead,cb_wasRecorded wasRecoded) = 0;
};


#endif /* ICONTAINER_HPP_ */
