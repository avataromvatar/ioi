/*
 * Container.hpp
 *
 *  Created on: Sep 29, 2020
 *      Author: avatar
 *
 */

#ifndef CONTAINER_HPP_
#define CONTAINER_HPP_

#include "ioi/Container/IContainer.hpp"
#include "iostream"
#include "vector"
#include "memory"

#define MULTITHREAD_CONTAINER
#ifdef MULTITHREAD_CONTAINER
#include "thread"
#include "mutex"
#endif


//struct FixedBufferPointer
//{
//    char *BeginPosition; 	//указатель на начало буффера
//    char *CurrentPosition;  //Текущая позиция
//    char *EndPosition; //Конец буффера. для get определяет заполненость буффера данными, для put сколько можно  еще закинуть
//    std::streamsize maxSizeInByte;
//};


/**
 *
 */
class Container : public IContainer, public std::streambuf
{
  public:


    Container (int sizeInByte);
    Container (int sizeElementInByte, int numElements);

    virtual ~Container();
    virtual int getElementSize ();
    virtual int getMaxNumElementsInBuffer ();
    virtual int getAvailableDataToReadInByte ();
    virtual int getAvailableMemToWriteInByte ();
    virtual int getAvailableElementToRead ();
    virtual int getAvailableElementToWrite ();
    virtual int getFullSizeInByte ();
    virtual bool isEmpty ();
    virtual bool isFixed ();
    virtual bool isBroken ();
    virtual bool isPutSectionBlock ();
    virtual bool isGetSectionBlock ();

    /**
     * Запись данных в put section
     * @attention this is thread save method
     * \warning данные должны записоватся пакетами кратными elementSize
     * \warning кол-во байт записанных может отличатся от переданных для записи
     * @param data данные
     * @param lenInByte кол-во байт для записи
     * @return кол-во байт записанных или EOF если все плохо
     */
    virtual int write (void *data, int lenInByte);
    /**
     *
     * @attention this is thread save method
     * @param data
     * @param lenInByte
     * @return
     */
    virtual int read (void *data, int lenInByte);
    virtual std::shared_ptr<std::vector<char>> readAll ();


//    friend std::vector<char>&operator<<(std::vector<char>& left,  Container& right);

    /*------Non save method begin ----*/
    /**
     Получить прямой указаетель на текущее положение get section
    * \warning Вы получили прямой указатель на gptr все операции по чтению будут заблокированы до вызова consumeRead
    * \warning Проверте в начале кол-во доступных данных для чтения
    * @return указаетль на gptr
    */
    virtual const char* dataToRead ();
    /**
    * Получить прямой указаетель на текущее положение put section
    * \warning Вы получили прямой указатель на pptr все операции по записи будут заблокированы до вызова commitWrite
    * @return указаетль на pptr
    */
    virtual char* dataToWrite ();
    /**
     * Потвердить запись данных в put section и сместить указаетль pptr на lenInByte.
     * \warning Вызывает commitRead для оповещения о том что появились новые данные
     * @param lenInByte
     * @return
     */
    virtual int commitWrite (int lenInByte);
    /**
    * Потверждает что появились новые данные для чтения и указатель конца get смещается на lenInByte
    * Этот метод вызывается автоматически когда вызывается commitWrite или происходит любая запись данных
    * \warning call cb_wasRecorded
    * @param lenInByte кол-во байт добавленных для чтения
    * @return 0 если все хорошо -1 если нет свободного места
    */
    virtual int commitRead (int lenInByte);
    /**
    * Потверждает что данные в get были считаны и текущий указатель gptr смещается на  lenInByte.
    * call cb_wasRead
    * @param lenInByte кол-во байт считанных
    * @return  0 если все хорошо -1 если смещение станет больше чем указатель на конец get section
    */
    virtual int consumeRead(int lenInByte);
    /*------Non save method end ----*/

    virtual int flush (eFlushBufferSections type=ALL_SECTION);

    virtual void setCallback(cb_wasRead wasRead,cb_wasRecorded wasRecoded);

    static int linksTwoContainers(Container *c1,Container *c2);


  protected:




    int uflow ();
    ///Get character on underflow
    int underflow ();
    ///Get number of characters available
    std::streamsize showmanyc ();
    ///Get sequence of characters
    std::streamsize xsgetn (char *s, std::streamsize n);
    ///Put character back in the case of backup underflow
    int pbackfail (int c = EOF);

    ///PUT SECTION
    ///Put sequence of characters
    std::streamsize xsputn (const char *s, std::streamsize n);
    ///Put character on overflow
    int overflow (int c = EOF);

    int sync ();

    bool m_isEmpty = true;
    bool m_isBroken = false;
    bool m_isPutSectionBlock = false;
    bool m_isGetSectionBlock= false;
    bool m_isFixed = true;
//    bool m_isComplex = false; //это значит что его создали с разными указателями и внешними ссылками типа m_externGet
    IContainer *m_externGetContainer= NULL;
  private:
    char *m_buffer;
    std::streamsize m_sizePutSection;
    std::streamsize m_numElementsInPutSection;
    std::streamsize m_sizeGetSection;
    std::streamsize m_numElementsInGetSection;
    std::streamsize m_elementSize;
    cb_wasRead m_wasReadCB=NULL;
    cb_wasRecorded m_wasRecodedCB=NULL;

#ifdef MULTITHREAD_CONTAINER
    std::mutex mutex;

    std::thread::id threadID_toWrite;
    std::thread::id threadID_toRead;
#endif

};


// std::vector<char>&operator<<(std::vector<char>& left, const Container& right)
//{
//
//
//
//  return left;
//}


#endif /* CONTAINER_HPP_ */
