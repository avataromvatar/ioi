/*
 * port_test.cpp
 *
 *  Created on: Sep 30, 2020
 *      Author: avatar
 */




#include "../Container/Container.hpp"
#include "../Port/Port.hpp"
#include "vector"
bool port_test ()
{
//  Port p;
  Container c1(1,16);
  Container c2(1,16);
  Container::linksTwoContainers(&c1, &c2);

  std::istream is1 (&c1);
  std::istream is2 (&c2);
  std::ostream os1 (&c1);
  std::ostream os2 (&c2);

  printf("Wc1 %d ! Rc2 %d\n",c1.getAvailableMemToWriteInByte(),c2.getAvailableDataToReadInByte());
    printf("Wc2 %d ! Rc1 %d\n",c2.getAvailableMemToWriteInByte(),c1.getAvailableDataToReadInByte());
  os1<<"Hi";
  printf("Wc1 %d ! Rc2 %d\n",c1.getAvailableMemToWriteInByte(),c2.getAvailableDataToReadInByte());
  printf("Wc2 %d ! Rc1 %d\n",c2.getAvailableMemToWriteInByte(),c1.getAvailableDataToReadInByte());
  os2<<"Wellcome!";
  printf("Wc1 %d ! Rc2 %d\n",c1.getAvailableMemToWriteInByte(),c2.getAvailableDataToReadInByte());
  printf("Wc2 %d ! Rc1 %d\n",c2.getAvailableMemToWriteInByte(),c1.getAvailableDataToReadInByte());

  std::cout<<&c1<<std::endl;
  printf("Wc1 %d ! Rc2 %d\n",c1.getAvailableMemToWriteInByte(),c2.getAvailableDataToReadInByte());
    printf("Wc2 %d ! Rc1 %d\n",c2.getAvailableMemToWriteInByte(),c1.getAvailableDataToReadInByte());
    std::cout<<&c2<<std::endl;
    printf("Wc1 %d ! Rc2 %d\n",c1.getAvailableMemToWriteInByte(),c2.getAvailableDataToReadInByte());
        printf("Wc2 %d ! Rc1 %d\n",c2.getAvailableMemToWriteInByte(),c1.getAvailableDataToReadInByte());

  //  std::cout<<p.m_portWorkContainer<<" read available "<<p.getAvailableDataToReadInByte()
//      <<" | "<<p.m_portWorkContainer->getAvailableDataToReadInByte()<<std::endl;
//  os2<<"Wellcome!";
//  os1<<"1";
//  std::cout<<p.m_portWorkContainer<<" read available "<<p.getAvailableDataToReadInByte()
//        <<" | "<<p.m_portWorkContainer->getAvailableDataToReadInByte()<<std::endl;
//  std::cout<<"SDfdsffs"<<std::endl;
//  std::cout<<&p<<std::endl;
//  std::cout<<"p.m_portWorkContainer"<<"read available "<<p.getAvailableDataToReadInByte()
//        <<" | "<<p.m_portWorkContainer->getAvailableDataToReadInByte()<<std::endl;


  return true;
}
