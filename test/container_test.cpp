/*
 * container_test.cpp
 *
 *  Created on: Sep 29, 2020
 *      Author: avatar
 */

#include "../Container/Container.hpp"
#include "vector"
bool container_test ()
{
  Container c1 (10);
  Container c2 (3, 3);
  std::istream is1 (&c1);
  std::istream is2 (&c2);
  std::ostream os1 (&c1);
  std::ostream os2 (&c2);

  std::cout << "Провекрка записи данных размером в elementSize:" << std::endl;
  os1 << "1234";
  std::cout << " -- Записано 4 байта(" << "1234"
      << ") в контейнер с elementSize = 1 ";
  if (c1.getFullSizeInByte () - c1.getAvailableMemToWriteInByte () != 4)
    {
      std::cout << " = ПРОВАЛ " << std::endl;
      return false;
    }
  else
    std::cout << " = УСПЕХ " << std::endl;
  os2 << "666";
  std::cout << " -- Записано 3 байта (" << "666"
      << ")в контейнер с elementSize = 3 ";
  if (c2.getFullSizeInByte () - c2.getAvailableMemToWriteInByte () != 3)
    {
      std::cout << " = ПРОВАЛ " << std::endl;
      return false;
    }
  else
    std::cout << " = УСПЕХ " << std::endl;
  std::cout << "Провекрка чтения данных размером в elementSize:" << std::endl;
  int test1 = 0;
  int test2 = 0;
  is1 >> test1;
  is2 >> test2;
  std::cout << " -- Чтение 4 байта из контейнер с elementSize = 1 ";
  if (test1 != 1234)
    {
      std::cout << " = ПРОВАЛ " << std::endl;
      return false;
    }
  else
    std::cout << " = УСПЕХ " << std::endl;
  std::cout << " -- Чтение 3 байта из контейнер с elementSize = 3 ";
  if (test2 != 666)
    {
      std::cout << " = ПРОВАЛ " << std::endl;
      return false;
    }
  else
    std::cout << " = УСПЕХ " << std::endl;
  std::cout << "Провекрка что в get section нет данных" << std::endl;
  if (c1.getAvailableDataToReadInByte () == 0
      && c2.getAvailableDataToReadInByte () == 0)
    {
      std::cout << " = УСПЕХ " << std::endl;

    }
  else
    {
      std::cout << " = ПРОВАЛ " << std::endl;
      return false;
    }
  std::cout << "Провекрка что в put section нет данных " <<c1.getAvailableMemToWriteInByte ()<<" | "<<c1.getFullSizeInByte ()<<" | "
      <<c2.getAvailableMemToWriteInByte ()<<" | "<<c2.getFullSizeInByte ()<<std::endl;
  if (c1.getAvailableMemToWriteInByte () == c1.getFullSizeInByte ()
      && c2.getAvailableMemToWriteInByte () == c2.getFullSizeInByte ())
    {
      std::cout << " = УСПЕХ " << std::endl;

    }
  else
    {
      std::cout << " = ПРОВАЛ " << std::endl;
      return false;
    }
  std::cout
      << "Провекрка переполнения буфера при записи '123456789012345' и манипулирования данными"
      << std::endl;
  os1 << "123456789012345";
  os2 << "123456789012345";
  std::vector<char> v1 ((c1.dataToRead ()),
			c1.dataToRead () + c1.getAvailableDataToReadInByte ());
  std::vector<char> v2 ((c2.dataToRead ()),
			c2.dataToRead () + c2.getAvailableDataToReadInByte ());

//  std::cout<<"size:"<<v1.size()<<" data:"<<v1.data()<<std::endl;
//  std::cout<<v2.data()<<std::endl;
//  std::cout<<v3.data()<<std::endl;
//  std::cout<<v4.data()<<std::endl;


  if (c1.getAvailableDataToReadInByte () == c1.getFullSizeInByte ()
      && c2.getAvailableDataToReadInByte () == c2.getFullSizeInByte ())
    {
          std::cout << " = УСПЕХ " << std::endl;

        }
      else
        {
          std::cout << " = ПРОВАЛ " << std::endl;
          return false;
        }
  std::cout<<&c1<<std::endl;
    std::cout<<&c2<<std::endl;
    if (c1.getAvailableDataToReadInByte () == 0
          && c2.getAvailableDataToReadInByte () == 0)
        {
              std::cout << " = УСПЕХ " << std::endl;

            }
          else
            {
              std::cout << " = ПРОВАЛ " << std::endl;
              return false;
            }
    return true;
}
