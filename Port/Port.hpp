/*
 * Port.hpp
 *
 *  Created on: Sep 30, 2020
 *      Author: avatar
 */

#ifndef PORT_HPP_
#define PORT_HPP_

#include "ioi/Container/Container.hpp"

class Port:public Container
{
  public:
    Port ();
    Port (int sizeElementInByte, int numElements );
    virtual ~Port ();



  protected:
    Container *m_portWorkContainer;

};

#endif /* PORT_HPP_ */
