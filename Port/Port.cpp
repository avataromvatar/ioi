/*
 * Port.cpp
 *
 *  Created on: Sep 30, 2020
 *      Author: avatar
 */

#include "ioi/Port/Port.hpp"





Port::Port (): Container(1, 4096)
{
  // TODO Auto-generated constructor stub
//  setp(0,0);
//  FixedBufferPointer fp = {.BeginPosition = pbase(),.CurrentPosition = pbase(),.EndPosition = pbase(),.maxSizeInByte = getFullSizeInByte()};
//  m_portWorkContainer = new Container(1,4096,fp,this);
//  setg(m_portWorkContainer->eback(),m_portWorkContainer->eback(),m_portWorkContainer->eback());
//  m_externGetContainer = m_portWorkContainer;
  m_portWorkContainer = new Container(1,4096);
  Container::linksTwoContainers(this, m_portWorkContainer);

}
Port::Port (int sizeElementInByte, int numElements ): Container(sizeElementInByte, numElements)
{
  m_portWorkContainer = new Container(sizeElementInByte,numElements);
   Container::linksTwoContainers(this, m_portWorkContainer);
}

Port::~Port ()
{
  // TODO Auto-generated destructor stub
  delete m_portWorkContainer;
}

